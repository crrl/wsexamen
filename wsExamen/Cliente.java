package wsExamen;

public class Cliente {
	int cod_cliente;
	String nombre;
	char membresia;
	Video [] videos;
	int vdoIndex;
	
	public Cliente (int cod_cliente, String nombre, char membresia){
		this.cod_cliente=cod_cliente;
		this.nombre=nombre;
		this.membresia=membresia;
		videos= new Video[10];
		vdoIndex = 0;
		
	}
	
	public void rentar(Video video){
		
		videos[vdoIndex] = video;
		vdoIndex++;
	}
	
	public float totalRenta(){
		float sumatoria=0;
		
		for(int i =0; i<vdoIndex;i++){
			sumatoria+=videos[i].precio;
		}
		
		return sumatoria;
	}
	
	/*public float totalRentaMembresia(char m){
		float sumatoria=0;
		
		for(int i =0; i<vdoIndex;i++)
			if(membresia==m)
				sumatoria+=videos[i].precio;
		
		return sumatoria;
	}*/
	
	public void mostrarRentas(){
		
		for(int i=0;i<vdoIndex;i++){
			
			System.out.println(videos[i].nombre);
		
		}
	}
}
