package wsExamen;

import java.util.Scanner;

import javax.swing.plaf.synth.SynthStyle;

import java.util.Arrays;

public class Main {
	
	static Scanner in = new Scanner(System.in);
	static Cliente [] clientes = new Cliente[100];
	static Video [] videos = new Video[100];
	static int cteIndex = 0;
	static int vdoIndex= 0;
	
	public static void main(String [] args){
		altaCliente(cteIndex+1,"Mario",'n');
		altaCliente(cteIndex+1,"Jose Luis",'g');
		
		altaVideo(vdoIndex+1,"crepusculo",10,'B');
		altaVideo(vdoIndex+1,"xmen",100,'B');
		altaVideo(vdoIndex+1,"principito",50,'A');
		
		while(true){
			int opc = menu();
			
			if(opc == 0)
				break;
			else if(opc<1 || opc>6){
				System.out.println("***Seleccione una opcion valida***");
				continue;
			}
			
			switch(opc){
				case 1:{
					System.out.println("Proporcione nombre");
					String nombre = in.nextLine();
					in.next();
					System.out.println("Proporcione membresia (Normal n, Gold g)");
					char membresia = in.next().charAt(0);
					altaCliente(cteIndex+1, nombre, membresia);
					break;
				}
				case 2:{
					System.out.println("Proporciona el nombre");
					String nombre = in.nextLine();
					in.next();
					System.out.println("Proporcione Precio");
					float precio = in.nextFloat();
					System.out.println("Proporcione tipo A=Infantil, B=Adolescentes,C=Adultos");
					char tipo = in.next().charAt(0);
					altaVideo(vdoIndex+1,nombre,precio,tipo);
					System.out.println(Arrays.toString(videos));
					break;
				}
				
				case 3:{
					rentarVideo();
					break;
				}
				
				case 4:{
					int idCliente= seleccionarCliente();
					System.out.println("Total de: "+clientes[idCliente].nombre);
					System.out.println("$"+clientes[idCliente].totalRenta());
					break;
				}
				
				case 5:{
					float sumatoria=0;
					
					for(int i=0;i<cteIndex;i++)
						sumatoria+=clientes[i].totalRenta();
			
					System.out.println("Total del dia: $"+sumatoria);
				}
				
				case 6:{
					float sumatoriaN=0, sumatoriaG=0;
					for(int i=0;i<cteIndex;i++)
						if(clientes[i].membresia=='n')
							sumatoriaN+=clientes[i].totalRenta();
						else
							sumatoriaG+=clientes[i].totalRenta();

					System.out.println("Total de renta por membresia normal $"+sumatoriaN);
					System.out.println("Total de renta por membresia gold $"+sumatoriaG);
			
				}
			}
		}
	}
	
	public static int menu(){
		int x;
		System.out.println("Seleccione una opcion por favor:");
		System.out.println("1.-Dar de alta Cliente.");
		System.out.println("2.-Dar de alta Video.");
		System.out.println("3.-Rentar Video.");
		System.out.println("4.-Calcular Total adeudo por cliente.");
		System.out.println("5.-Calcular Total de renta.");
		System.out.println("6.-Calcular Total de renta por membrecia.");
		System.out.println("0. -Salir.");
		
		return in.nextInt();
	}
	
	public static void altaCliente(int cod_cliente, String nombre, char membresia){
		
		Cliente cliente = new Cliente(cod_cliente,nombre,membresia);
		
		clientes[cteIndex]=cliente;
		cteIndex++;
	}
	
	public static void altaVideo(int id, String nombre, float precio, char tipo){
		
		Video video = new Video(id, nombre, precio, tipo);

		videos[vdoIndex]=video;
		vdoIndex++;
	}
	
	public static int seleccionarCliente(){
		for(int i=0; i<cteIndex; i++){
			Cliente cte = clientes[i];
			System.out.println("id:"+cte.cod_cliente+"- "+cte.nombre);
		}
		
		System.out.println("Capture el id del cliente");
		int id = in.nextInt();
		
		return id-1;
	}
	
	public static int seleccionarVideo(){
		for(int i=0; i<vdoIndex; i++){
			Video vid = videos[i];
			System.out.println("id:"+vid.id+"- "+vid.nombre +"\t\t$"+vid.precio);
		}
		
		System.out.println("Capture el id del video");
		int id = in.nextInt();
		
		return id-1;
	}
	
	public static void rentarVideo(){
		
		int idCliente = seleccionarCliente();
		int idVideo = seleccionarVideo();
		
		clientes[idCliente].rentar(videos[idVideo]);
		clientes[idCliente].mostrarRentas();
	}
}
